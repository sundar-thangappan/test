#This is files for Laravel 5.4 - PHP 

1. Place the <date>_create_doctors_table.php migration file in migrations folder (database\migrations).
	Run migrate command to create the doctors table. (php artisn migrate)
	Now add some seed data to the database table - doctors.

2.The corresponding model for the table is Doctor.php. Place it in the app folder.

2. Add the web.php 's code to the first line of your web.php (routes\web.php).

3. Place the DoctorController.php controller in your Controllers folder (app\Http\Controllers). 

4. Place the doctor.blade.php in you views folder (resources\views).

5. Now hit <domain>/show_doctors.