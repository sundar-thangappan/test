<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Doctor;
use Illuminate\Support\Facades\Log; //

class DoctorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the doctor dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $intervalDays = 30; //previous days
        $maxRecord = 150; //max. records to select

        $today = date('Y-m-d');
        $prevDay = new \DateTime($today);
        $prevDay = $prevDay->sub(new \DateInterval("P{$intervalDays}D"));
        $prevDay = $prevDay->format('Y-m-d') . ' 00:00:00';
        $today .= ' 23:59:59';

        $doctors = Doctor::getDoctors( $prevDay, $today, $maxRecord );

        return view('doctor')->with('doctors', $doctors);
    }
}
