@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Doctors</div>

                <div class="panel-body">
                    <div class="col-sm-12">

                    <table class="table">
                      <thead class="thead-default">
                        <tr>
                          <th>#</th>
                          <th>Doctor Name</th>
                          <th>Hospital Name</th>
                          <th>Qualification</th>
                          <th>Location</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ( $doctors as $index => $doctor)
                            <tr>
                              <th scope="row">{{ $index+1 }}</th>
                              <td>{{ $doctor['doctor_name'] }}</td>
                              <td>{{ $doctor['hospital_name'] }}</td>
                              <td>{{ $doctor['qualification'] }}</td>
                              <td>{{ $doctor['location'] }}</td>
                            </tr>
                        @endforeach
                      </tbody>
                    </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
